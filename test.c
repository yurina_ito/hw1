#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define WORD_LENGTH 100

typedef struct data_t{
  char *word;     
  int  alphabet[26];//各アルファベットの出てくる回数を保持する配列
} data_type;

data_type table[2];
int length;

void count(char *info,char *counter){ //文字列に含まれるアルファベットの数をカウントする
  char c;
  char strcnt[100];
  int cnt[26];
  int i,num=0;
  
  strcpy(strcnt,info);

  for(i=0;i<26;i++){
    cnt[i]=0;
  }
  for(i=0;strcnt[i]!='\0';i++){
    c=tolower(strcnt[i])-'a';
    cnt[c]++;
  }

//入力された文字のアルファベット数と単語に含まれている数を比べる
  for(i=0;i<26;i++){          
    if(cnt[i]>counter[i]){
      return;
      }
  }

 //単語が入力されたアルファベットを並び替えてできるものならtable[1]に代入
    table[1].word=info;
    for(i=0;i<26;i++){
      table[1].alphabet[i]=cnt[i];    
    }
    compare();
  
}


void read_file(char *filename,char *counter){  //ファイルを読み込む関数
  FILE *file;
  int r;
  char *str;

  file=fopen(filename,"r");
  if(file==NULL){
    printf("NOT_FOUND\n",filename);
    exit(1);
  }
  while(1){
    str=(char*)malloc(WORD_LENGTH);  //単語を読み込むメモリを確保
    r=fscanf(file,"%s\n",str);     //１行ずつデータを読み込む
    if(r==EOF){
      fclose(file);
      return;
    }else{
      count(str,counter);
    }
  }
}

//table内に入っている文字列の長さを比べて、table[0]にその時点で一番長い単語を入れる
int compare(){
  int i;

  if(length<strlen(table[1].word)){
    length=strlen(table[1].word);
    table[0].word=table[1].word;
  }
}



int main(){        
  char anagram[100];
  char counter[26];
  int i,num;
  char c;

  length=0;

  printf("please input 16 characters:");//16文字を受け取る
  scanf("%s",anagram);
  
  for(i=0;i<26;i++){        //文字列に含まれるアルファベットをカウント
    counter[i]=0;
  }
  
  for(i=0;anagram[i]!='\0';i++){
    c=tolower(anagram[i])-'a';
    counter[c]++;
  }

  read_file("example.txt",counter);

  if (table[0].word==NULL){
    printf("can not make word from %s\n",anagram);
  }else{
  printf("most longest word is %s\n",table[0].word);
  }

}
